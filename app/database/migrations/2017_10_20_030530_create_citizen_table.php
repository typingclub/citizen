<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitizenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('citizen', function($t) {
            $t->engine ='InnoDB';
            $t->increments('id')->unsigned();
            $t->string('name', 128);
            $t->string('cpf', 64);
            $t->string('email', 64);
            $t->boolean('victim_maria_penha');
            $t->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('citizen');
	}

}
