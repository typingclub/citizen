<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Citizen extends Eloquent {
    
    protected $table = 'citizen';

    protected $fillable = array('name', 'cpf', 'email', 'victim_maria_penha');
}