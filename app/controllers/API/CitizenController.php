<?php namespace API;

use Illuminate\Routing\Controllers\Controller;

use Input, Response, URL, Validator;

use Citizen as CitizenModel;

class CitizenController extends \BaseController {

    /*Show List Paginated*/
	public function index() {

        $pageNo = Input::get('pageNo');
        $pageLimit = Input::get('limit');

        $citizens = CitizenModel::all();

        if ($pageNo) {
            $citizens = CitizenModel::take($pageLimit)->offset(($pageNo - 1) * $pageLimit)->get();
        }

        return Response::json(['result' => 'success', 'citizens' => $citizens]);

	}

	/*Create Citizen*/
	public function create() {
        $validator = Validator::make(Input::all(), [
            "name"    => "required|max:50",
            "cpf" => "required|max:50",
            "email"     => "required|email",
            "victim_maria_penha"   => "required|boolean"
        ]);

        if ($validator->passes()) {
            $citizen = new CitizenModel;
            $citizen->fill(Input::all());
            $citizen->save();

            return Response::json(['result' => 'success', 'citizen_id' => $citizen->id]);
        }

    }

    /*Show a Citizen*/
    public function show($citizenId) {
        $citizen = CitizenModel::find($citizenId);

        return Response::json(['result' => 'success', 'citizen' => $citizen]);
    }

    /*Update a Citizen*/
    public function update($citizenId) {

        $validator = Validator::make(Input::all(), [
            "name"    => "required|max:50",
            "cpf" => "required|max:50",
            "email"     => "required|email",
            "victim_maria_penha"   => "required|boolean"
        ]);

        if ($validator->passes()) {
            $citizen = CitizenModel::find($citizenId);

            $citizen->fill(Input::all());
            $citizen->save();

            return Response::json(['result' => 'success', 'citizen' => $citizen]);
        }
    }

    /*Delete a Citizen*/
    public function destroy($citizenId) {
        CitizenModel::find($citizenId)->delete();

        return Response::json(['result' => 'success']);
    }
}
