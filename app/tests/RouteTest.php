<?php

class ExampleTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testBasicExample()
	{
		$crawler = $this->client->request('GET', '/');

		$this->assertTrue($this->client->getResponse()->isOk());
	}

	public function testListCitizen()
    {
        $response = $this->call('GET', 'api/citizen');
        $this->assertTrue($response->isOk());
        dd($response->getContent());
    }

    public function testCitizenCreate() {
        $response = $this->call('POST', 'api/citizen', ['name'=> 'unittest', 'email' => 'unittest@gmail.com', 'cpf'=>'test123123', 'victim_maria_penha'=> '1']);
        $this->assertTrue($response->isOk());
        dd($response->getContent());
    }

}
